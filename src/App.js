import React from "react";
import { Button } from "react-md";
import AddSound from "./Components/AddSound";
import AudioPlayer from "react-h5-audio-player";
import { observer } from "mobx-react-lite";
import { backendWsStore } from "./store/BackendWs";
import { myMessagePlayer } from "./store/MessagePlayer";
import "./App.css";
import "react-h5-audio-player/lib/styles.css";

const MPlayerView = observer(() => {
  return (
    <div>
      <AudioPlayer
        showSkipControls
        src={myMessagePlayer.currentMessage}
        autoplay
        onClickNext={myMessagePlayer.checkNext}
        onEnded={myMessagePlayer.checkNext}
      />
      <li>{myMessagePlayer.messageList[0]}</li>
    </div>
  );
});

export const App = () => {
  const { backendWs } = backendWsStore;

  const resetAttempts = () => {
    let reset_payload = { command_type: "reset_attempts", whoami: "mainp" };
    backendWs.send(JSON.stringify(reset_payload));
  };
  const updateLootler = () => {
    let lootler_payload = { command_type: "update_loolter", whoami: "mainp" };
    backendWs.send(JSON.stringify(lootler_payload));
  };
  return (
    <div>
      <MPlayerView />
      <AddSound ws={backendWs}></AddSound>
      <Button raised="true" onClick={resetAttempts}>
        Reset Attempts
      </Button>
      <Button raised="true" onClick={updateLootler}>
        Update lootler
      </Button>
    </div>
  );
};
