import { action, makeObservable, observable } from "mobx";

class MessagePlayer {
  currentMessage = "";
  messageList = [];
  constructor() {
    makeObservable(this, {
      currentMessage: observable,
      messageList: observable,
      checkNext: action.bound,
      playMessage: action,
    });
  }

  checkNext() {
    console.log(this);
    let mod_messages = this.messageList;
    mod_messages.shift();
    if (mod_messages.length !== 0) {
      this.messageList = mod_messages;
      this.currentMessage = this.messageList[0];
    } else {
      this.messageList = mod_messages;
    }
  }

  playMessage(message) {
    let voicedMessage = URL.createObjectURL(message);
    if (this.messageList.length === 0) {
      this.currentMessage = voicedMessage;
      this.messageList = [voicedMessage];
      console.log(this.messageList);
    } else {
      this.messageList = [...this.messageList, voicedMessage];
    }
  }
}

export const myMessagePlayer = new MessagePlayer();
