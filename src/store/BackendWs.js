import { myMessagePlayer } from "./MessagePlayer";
class BackendWs {
  backendWs;
  connectionTimeout;

  constructor() {
    this.backendWs = this.connect();
  }

  connect() {
    if (this.connectionTimeout) {
      this.backendWs.close();
      clearInterval(this.connectionTimeout);
    }
    const ws = new WebSocket(`ws://${window.location.hostname}:3030`);
    ws.onopen = () => {
      let init_pl = { command_type: "init", whoami: "mainp" };
      ws.send(JSON.stringify(init_pl));
      console.log("connected");
    };

    ws.onmessage = (evt) => {
      let data = JSON.parse(evt.data);
      if (data.command_type === "chat_sound") {
        //this.playMessage(new Blob([new Uint8Array(data.sound_buf, 0, data.sound_buf.length)]));
        //myMessagePlayer.playMessage(new Blob([new Uint8Array(data.sound_buf, 0, data.sound_buf.length)]));
        myMessagePlayer.playMessage(
          new Blob([new Uint8Array(data.sound_buf, 0, data.sound_buf.length)])
        );
      }
    };
    ws.onclose = () => {
      console.log("disconnected... Reconnect in 3 seconds");
      this.connectionTimeout = setInterval(() => {
        this.backendWs = this.connect();
      }, 3000);
    };
    return ws;
  }
}

export const backendWsStore = new BackendWs();
