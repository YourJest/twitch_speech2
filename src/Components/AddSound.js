import React, { useState } from "react";
import { Button } from "@react-md/button";
import { Dialog, DialogHeader, DialogTitle } from "@react-md/dialog";
import { Form, TextField, FileInput, useFileUpload } from "@react-md/form";
import { useToggle } from "@react-md/utils";
import { backendWsStore } from "../store/BackendWs";

import styles from "./SimpleListExample.module.scss";
const extensions = ["ogg"];

export default function AddSound() {
  const { backendWs } = backendWsStore;

  const [toggled, enable, disable] = useToggle(false);
  const { stats, onChange } = useFileUpload({
    concurrency: 1,
    extensions,
    getFileParser: () => "readAsArrayBuffer",
  });
  const [regContent, setRegContent] = useState("");
  const [regFlags, setFlags] = useState("");
  function addNewSound() {
    let fileName = stats[0].file.name;
    let byteData = Array.from(new Uint8Array(stats[0].result));
    let sound_paylaod = {
      command_type: "add_sound",
      content: regContent,
      flags: regFlags,
      fileName: fileName,
      byteData: byteData,
      whoami: "mainp",
    };
    console.log(sound_paylaod);
    backendWs.send(JSON.stringify(sound_paylaod));
  }
  function changeContent(e) {
    setRegContent(e.target.value);
  }
  function changeFlags(e) {
    setFlags(e.target.value);
  }
  return (
    <>
      <Button id="dialog-toggle-1" onClick={enable}>
        Add Sound
      </Button>
      <Dialog
        id="dialog-1"
        visible={toggled}
        onRequestClose={disable}
        aria-labelledby="dialog-title"
      >
        <DialogHeader>
          <DialogTitle id="dialog-title">Add new Sound</DialogTitle>
        </DialogHeader>
        <Form className={styles.list}>
          <TextField
            id="regexp-content"
            label="Regexp Content"
            onChange={changeContent}
          />
          <TextField
            id="regexp-flags"
            label="Regexp Flag"
            onChange={changeFlags}
          />
          <FileInput id="configurable-file-input" onChange={onChange}>
            Upload puk
          </FileInput>
          <Button onClick={addNewSound}>Add sound</Button>
        </Form>
      </Dialog>
    </>
  );
}
